
# Remove Base Request Types Plugin for MSM

This plugin will hide all base request types from the new, convert request menus.

## Compatible Versions

| Plugin  | MSM                    |
|---------|------------------------|
| 1.0.0   | 14.3.0, 14.4.0, 14.5.0 |
| 1.0.1   | 14.13.0                |
| 1.0.1.7 | 15.1+                  |

## Installation

Please see your MSM documentation for information on how to install plugins.

There are no settings to configure once the plugin has been installed.

## Usage

The plugin finds and hides all default request types so the user is unable to create requests using them.

## Contributing

We welcome all feedback including feature requests and bug reports. Please raise these as issues on GitHub. If you would like to contribute to the project please fork the repository and issue a pull request.
